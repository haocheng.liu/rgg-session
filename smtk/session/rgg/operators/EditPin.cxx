//=============================================================================
// Copyright (c) Kitware, Inc.
// All rights reserved.
// See LICENSE.txt for details.
//
// This software is distributed WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE.  See the above copyright notice for more information.
//=============================================================================
#include "smtk/session/rgg/operators/EditPin.h"

#include "smtk/session/rgg/Pin.h"
#include "smtk/session/rgg/Resource.h"
#include "smtk/session/rgg/Session.h"
#include "smtk/session/rgg/json/jsonPin.h"

#include "smtk/session/rgg/operators/CreateModel.h"

#include "smtk/io/Logger.h"

#include "smtk/PublicPointerDefs.h"
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ModelEntityItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/attribute/VoidItem.h"
#include "smtk/model/AuxiliaryGeometry.h"
#include "smtk/model/Model.h"

#include "smtk/session/rgg/EditPin_xml.h"

#include <string> // std::to_string
using namespace smtk::model;
using json = nlohmann::json;

namespace smtk
{
namespace session
{
namespace rgg
{

EditPin::Result EditPin::operateInternal()
{
  EntityRefArray entities = this->parameters()->associatedModelEntities<EntityRefArray>();
  if (entities.empty() || (!entities[0].isAuxiliaryGeometry() && !entities[0].isModel()))
  {
    smtkErrorMacro(this->log(), "Cannot edit a non auxiliary geometry nor model");
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }

  // Get pin representation
  std::string pinRepStr;
  smtk::attribute::StringItemPtr pinRepStrItem = this->parameters()->findString("pin representation");
  if (pinRepStrItem != nullptr && !pinRepStrItem->value(0).empty())
  {
    pinRepStr = pinRepStrItem->value(0);
  }
  Pin pin = json::parse(pinRepStr);

  Result result = this->createResult(smtk::operation::Operation::Outcome::SUCCEEDED);
  if (entities[0].isModel())
  { // Creation mode
    EntityRef parent = entities[0];

    smtk::model::AuxiliaryGeometry newPinAux = parent.resource()->addAuxiliaryGeometry(
                                    parent.as<smtk::model::Model>(), 3);
    newPinAux.setName(pin.name());
    newPinAux.setStringProperty("label", pin.label());
    newPinAux.setStringProperty("rggType", Pin::typeDescription);
    newPinAux.setStringProperty(Pin::propDescription, pinRepStr);
    newPinAux.setColor(pin.color());

    // A list contains all subparts and layers of the pin
    std::vector<EntityRef> subAuxGeoms;
    this->populateChildrensInPinAux(newPinAux, subAuxGeoms, pin);

    result->findComponent("modified")->appendValue(parent.component());
    smtk::attribute::ComponentItem::Ptr createdItem = result->findComponent("created");
    createdItem->appendValue(newPinAux.component());
    for (auto& c : subAuxGeoms)
    {
      createdItem->appendValue(c.component());
    }
  }
  else
  { // Edit mode
    smtk::model::EntityRefArray expunged, modified, tobeDeleted;

    smtk::model::AuxiliaryGeometry pinAux = entities[0].as<AuxiliaryGeometry>();
    auto resource = std::dynamic_pointer_cast<smtk::session::rgg::Resource>(pinAux.resource());

    // Remove all current child auxiliary geometries first
    EntityRefArray children = pinAux.embeddedEntities<EntityRefArray>();
    std::cout << "Try to remove " << children.size() << " child auxgeoms" <<std::endl;
    tobeDeleted.insert(tobeDeleted.end(), children.begin(), children.end());

    pinAux.setName(pin.name());
    pinAux.setStringProperty("label", pin.label());
    pinAux.setStringProperty(Pin::propDescription, pinRepStr);
    pinAux.setColor(pin.color());

    // A list contains all subparts and layers of the pin
    std::vector<EntityRef> subAuxGeoms;
    this->populateChildrensInPinAux(pinAux, subAuxGeoms, pin);

    smtk::attribute::ComponentItem::Ptr createdItem = result->findComponent("created");
    for (auto& c : subAuxGeoms)
    {
      createdItem->appendValue(c.component());
    }
    smtk::attribute::ComponentItem::Ptr expungedItem = result->findComponent("expunged");
    for (auto& e : tobeDeleted)
    {
      std::cout << "EditPin Op: expunge " << e.name() << " with id as "
                << e.entity().toString() << std::endl;
      expungedItem->appendValue(e.component());
    }

    smtk::attribute::ComponentItem::Ptr modifiedItem = result->findComponent("modified");
    modifiedItem->appendValue(pinAux.component());

    // Delete child aux geoms after being been appended to expunged item since once they
    // are deleted, the component of each aux geom would become invalid.
    resource->deleteEntities(tobeDeleted, modified, expunged, this->m_debugLevel > 0);
  }
  return result;
}

void EditPin::populateChildrensInPinAux(smtk::model::AuxiliaryGeometry& pinAux,
                              std::vector<smtk::model::EntityRef>& subPinAuxs,
                                          const Pin& pin)
{
  auto assignColor = [](size_t index, smtk::model::AuxiliaryGeometry& aux) {
    smtk::model::FloatList rgba = smtk::session::rgg::CreateModel::getMaterialColor(index, aux.owningModel());
    aux.setColor(rgba);
  };
  size_t numParts = pin.pieces().size();
  size_t numLayers = pin.layerMaterials().size();

  // Create auxgeom placeholders for layers and parts
  for (std::size_t i = 0; i < numParts; i++)
  {
    for (std::size_t j = 0; j < numLayers; j++)
    {
      // Create an auxo_geom for current each unit part&layer
      AuxiliaryGeometry subLayer = pinAux.resource()->addAuxiliaryGeometry(pinAux, 3);
      std::string subLName = pin.name() + smtk::session::rgg::Pin::subpartDescription +
                             std::to_string(i) +
                             Pin::layerDescription + std::to_string(j);
      subLayer.setName(subLName);
      subLayer.setStringProperty("rggType", Pin::typeDescription);
      assignColor(static_cast<size_t>(pin.layerMaterials()[j].subMaterialIndex), subLayer);
      subPinAuxs.push_back(subLayer.as<EntityRef>());
    }
    if (pin.cellMaterialIndex()) // 0 index means no material
    {
      // Append a material layer after the last layer
      AuxiliaryGeometry materialLayer = pinAux.resource()->addAuxiliaryGeometry(pinAux, 3);
      std::string materialName =
        pin.name() + smtk::session::rgg::Pin::subpartDescription +
          std::to_string(i) + Pin::materialDescription;
      materialLayer.setName(materialName);
      materialLayer.setStringProperty("rggType", Pin::typeDescription);
      assignColor(size_t(pin.cellMaterialIndex()), materialLayer);
      subPinAuxs.push_back(materialLayer.as<EntityRef>());
    }
  }
}

const char* EditPin::xmlDescription() const
{
  return EditPin_xml;
}
} // namespace rgg
} //namespace session
} // namespace smtk
